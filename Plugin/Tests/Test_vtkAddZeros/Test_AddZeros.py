
import sys

print sys.argv

try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()


if(len(sys.argv) == 1) :
  sys.exit('I need the root path of the paraview to load the plugin')

print('Loading Pluing')
PluginPath = sys.argv[1];
paraview.servermanager.LoadPlugin(PluginPath + '/lib/libPXDMFTOOLS.so',True)
print('Loading Pluing DONE')

Disk1 = Disk()
Disk1.CircumferentialResolution = 230
Disk1.RadialResolution = 139
Disk1.UpdatePipeline()

Calc1 = Calculator(Input=Disk1)
Calc1.ResultArrayName = "Toto";
Calc1.Function = 'coordsX'
Calc1.UpdatePipeline()

#print(dir())

AddZeros1 = paraview.servermanager.filters.AddZeros( Input=Calc1 )
AddZeros1.PointArrays = ['Toto']
AddZeros1.UpdatePipeline()

if not (AddZeros1.PointData.GetArray(0).Name == (Calc1.ResultArrayName+"_0")):
  raise Exception("Add Zeros not Working")

