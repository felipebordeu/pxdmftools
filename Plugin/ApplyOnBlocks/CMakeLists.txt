set(SERVER_MANAGER_SOURCES_files  
      vtkApplyOnBlocksFilter.cxx
      vtkSIMetaFilterProxy.cpp
) 

set(SERVER_MANAGER_HEADERS_files 
      vtkApplyOnBlocksFilter.h
      vtkSIMetaFilterProxy.h
) 

set( SERVER_MANAGER_XML_files 
    ApplyOnBlocks.xml
)

vtk_module_add_module(PXDMFTools::ApplyOnBlocks
   FORCE_STATIC 
   SOURCES ${SERVER_MANAGER_SOURCES_files} 
   HEADERS ${SERVER_MANAGER_HEADERS_files}
   )
   
paraview_add_server_manager_xmls(
  XMLS  ${SERVER_MANAGER_XML_files})
  
