add_to_COPY_ONLY_FILES(
    TransformFilterWithAxis.png
    )
      
vtk_module_add_module(PXDMFTools::TransformWithAxis
   FORCE_STATIC 
   SOURCES vtkTransformFilterWithAxis.cxx
   HEADERS vtkTransformFilterWithAxis.h
   )
   
paraview_add_server_manager_xmls(
  XMLS  TransformFilterWithAxis.xml)
  
  
