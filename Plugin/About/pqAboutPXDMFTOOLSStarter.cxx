/*=========================================================================

  Program:   PXDMFTOOLS Plugin
  Module:    pqAboutPXDMFTOOLSStarter.cxx

  Copyright (c) PXDMFTOOLS, Ecole Centrale Nantes.
  All rights reserved.
  Copyright: See COPYING file that comes with this distribution


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "pqAboutPXDMFTOOLSStarter.h"

// Qt Includes.
#include <QSplashScreen>
#include <QBitmap>
#include "QTimer"

// ParaView Includes.
#include "pqSettings.h"
#include "pqApplicationCore.h"

//plugin Includes
#include "AxesLabels.h"
#include "PostFilters.h"


pqAboutPXDMFTOOLSStarter ::pqAboutPXDMFTOOLSStarter ( QObject *p )  : QObject ( p ) {
    this->Splash = NULL;


    };
//-----------------------------------------------------------------------------
pqAboutPXDMFTOOLSStarter ::~pqAboutPXDMFTOOLSStarter() {
    if ( this->Splash ) {
        }
    };
//-----------------------------------------------------------------------------
std::string pqAboutPXDMFTOOLSStarter::version = "@PXDMFTools_VERSION_MAJOR@.@PXDMFTools_VERSION_MINOR@.@PXDMFTools_VERSION_PATCH@";
//Callback for startup;
void pqAboutPXDMFTOOLSStarter::onStartup() {
   
    this->AxesLabelsB = new pqAxesLabels(this);
    this->PostFiltersB = new pqPostFilters(this);

    pqSettings *settings = pqApplicationCore::instance()->settings();

    bool show = settings->value ( "PxdmfSettings.ShowSplashScreen",true ).toBool();
    show = false;
    if ( show ) {
        QTimer::singleShot ( 3000, this, SLOT ( show() ) );
        }
    };
//
void pqAboutPXDMFTOOLSStarter::show() {
    QPixmap pixmap ( ":/PXDMFTOOLSPanel/splash.png" );
    this->Splash = new QSplashScreen ( pixmap, Qt::WindowStaysOnTopHint );
    this->Splash->setMask ( pixmap.createMaskFromColor ( QColor ( Qt::transparent ) ) );
    this->Splash->showMessage ( "https://github.com/fbordeu/PxdmfToolsForParaView \n V. @PXDMFTOOLS_VERSION_MAJOR@.@PXDMFTOOLS_VERSION_MINOR@.@PXDMFTOOLS_VERSION_PATCH@ Compilation Flags @PXDMFTOOLS_COMPILE_FLAGS@", Qt::AlignBottom | Qt::AlignHCenter );
    this->Splash->show();
    QTimer::singleShot ( 5000, this, SLOT ( close() ) );
    }
//
void pqAboutPXDMFTOOLSStarter::close() {
    if ( this->Splash ) {
        this->Splash->close();
        this->Splash = 0;
        }
    }
//Callback for Shutdown;
void pqAboutPXDMFTOOLSStarter::onShutdown() {
      delete this->AxesLabelsB;
      delete this->PostFiltersB; 
  
    }
//

