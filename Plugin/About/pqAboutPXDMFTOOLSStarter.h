/*=========================================================================

  Program:   PXDMFReader Plugin
  Module:    pqAboutPXDMFTOOLSStarter.h

  Copyright (c) PXDMFTOOLS, Ecole Centrale Nantes.
  All rights reserved.
  Copyright: See COPYING file that comes with this distribution


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME pqAboutPXDMFTOOLSStarter
// .SECTION Description

#ifndef __pqAboutPXDMFTOOLSStarter_h
#define __pqAboutPXDMFTOOLSStarter_h

// Qt Includes.
#include <QObject>

class QSplashScreen;
class pqPipelineSource;
class pqAxesLabels;
class pqPostFilters;

class pqAboutPXDMFTOOLSStarter : public QObject {
    Q_OBJECT
    typedef QObject Superclass;
    QSplashScreen* Splash;
protected  slots:
    void close();
    void show();

public:
    pqAboutPXDMFTOOLSStarter(QObject *p=0) ;
    ~pqAboutPXDMFTOOLSStarter();

//Callback for startup;
    void onStartup();

//Callback for Shutdown;
    void onShutdown();

    static std::string version;

    //behaviours
private :
    pqAxesLabels* AxesLabelsB;
    pqPostFilters* PostFiltersB;
private:
    pqAboutPXDMFTOOLSStarter(const pqAboutPXDMFTOOLSStarter&); // Not implemented.
    void operator=(const pqAboutPXDMFTOOLSStarter&); // Not implemented.
};

#endif
