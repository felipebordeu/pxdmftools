#----------------------------------------------------------------
# About configuration
#----------------------------------------------------------------
IF(PARAVIEW_BUILD_QT_GUI)


#configure_file(
#    pqAboutPXDMFTOOLSStarter.cxx.in 
#    "${CMAKE_CURRENT_BINARY_DIR}/pqAboutPXDMFTOOLSStarter.cxx"
#    @ONLY)

add_SOURCES(#"${CMAKE_CURRENT_BINARY_DIR}/" 
            pqAboutPXDMFTOOLSStarter.cxx
            pqAboutPXDMFTOOLS.cxx 
            PXDMFAboutHelpMenuActions.cxx)

#----------------------------------------------------
#-------- Splash Screen of the plugin ---------------
#----------------------------------------------------
   
add_to_QT_WRAP_CPP(pqAboutPXDMFTOOLSStarter.h
                    pqAboutPXDMFTOOLS.h 
                    PXDMFAboutHelpMenuActions.h
                    )

#add_to_QT_WRAP_UI( AboutPXDMF.ui )


ENDIF(PARAVIEW_BUILD_QT_GUI)
