/*=========================================================================

  Program:   PXDMFReader Plugin
  Module:    pqAboutPXDMFTOOLS.h

  Copyright (c) PXDMFTOOLS, Ecole Centrale Nantes.
  All rights reserved.
  Copyright: See COPYING file that comes with this distribution


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vpqAboutPXDMFTOOLS
// .SECTION Description

#ifndef __pqAboutPXDMFTOOLS_h
#define __pqAboutPXDMFTOOLS_h

// Qt Includes.
#include <QDialog>
#include "ui_AboutPXDMF.h"


class pqAboutPXDMFTOOLS : public QDialog, private Ui::AboutPXDMF{
  Q_OBJECT
public:
  pqAboutPXDMFTOOLS (QDialog * parent = 0):QDialog(parent, Qt::Window | Qt::WindowStaysOnTopHint) {
    onAboutWindows();
    
  }
  //Callback to plot the about 
  void onAboutWindows();
public slots:
  void SavePDFFile();
};

#endif
