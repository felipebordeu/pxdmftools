
add_SERVER_MANAGER_SOURCES( 
     vtkReconstruction.h
     vtkReconstruction.cxx
     )
     
#IF(PARAVIEW_BUILD_QT_GUI)

    
#paraview_plugin_add_property_widget(
#    KIND WIDGET
#    TYPE "PointCellDataSelector"
#    CLASS_NAME "pqPointCellDataSelector"
#    INTERFACES property_interfaces
#    SOURCES property_sources
#    )

#list(APPEND interfaces ${property_interfaces})
#list(APPEND sources ${property_sources})



# add_to_QT_WRAP_UI(   pqPointCellDataSelector.ui 
#		       pqSpaceTimeSelector.ui
# )

# #ENDIF(PARAVIEW_BUILD_QT_GUI)

vtk_module_add_module(PXDMFTools::Qtsources
   SOURCES ${sources} #
#   HEADERS vtkPXDMFGeneralSettings.h
   )
#pqComputeDerivativesWidgetDecorator.cxx     pqComputeDerivativesWidgetDecorator.h   

  
