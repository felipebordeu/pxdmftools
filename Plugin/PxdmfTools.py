#=========================================================================
#
#  Program:   PXDMFReader Plugin
#  Module:    ReaderSync/py
#
#  Copyright (c) GeM, Ecole Centrale Nantes.
#  All rights reserved.
#  Copyright: See COPYING file that comes with this distribution
#
#
#     This software is distributed WITHOUT ANY WARRANTY; without even
#     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#     PURPOSE.  See the above copyright notice for more information.
#
#=========================================================================

from paraview.simple import *
import paraview.vtk

class ReaderSync():
  """ Class to synchronize PXDMF Readers
    import ReaderSync
    Pxdmfsync = ReaderSync.ReaderSync()
    Pxdmfsync.SetFixedDimension('Amp', 1.2)
    Pxdmfsync.SetFixedDimensionPer('DX', 0.2)
  """
  def __init__(self):
    self.UpdateSources()
    self.printstate = 0;
  
  def UpdateSources(self):
    """Update the internal list of PXDMF Readers"""
    self.mins = {}
    self.maxs = {}
    self.sources = GetSources()
    self.nbSources = len(self.sources)
    self.PXDMFReaders = []
    for i in range(self.nbSources):
      mysource = self.sources[self.sources.keys()[i]]
      if(isinstance(mysource, paraview.servermanager.sources.PXDMFReader )):
        self.PXDMFReaders.append(mysource)
        rmins = mysource.GetPropertyValue('PXDMFDimsMinRangeDataInfo')
        rmaxs = mysource.GetPropertyValue('PXDMFDimsMaxRangeDataInfo')
        names = mysource.GetPropertyValue('PXDMFDimsNameDataInfo')
        nbdims = len(names)
        for dim in range(nbdims):
          if self.mins.has_key(names[dim]):
            self.mins[names[dim]] = min(self.mins[names[dim]],rmins[dim])
            self.maxs[names[dim]] = min(self.maxs[names[dim]],rmaxs[dim])
          else:
            self.mins[names[dim]] = rmins[dim]
            self.maxs[names[dim]] = rmaxs[dim]

  def SetFixedDimension(self, name, value):
    """Set the value of a fixed coordinate """
    if not self.mins.has_key(name):
      return
    
    for mysource in self.PXDMFReaders:
      fixeddims = mysource.GetProperty('FixedDimensions')
      names = mysource.GetPropertyValue('PXDMFDimsNameDataInfo')
      for dim in range(len(names)):
        if names[dim] == name:
          fixeddims[dim] = value
          
  def SetFixedDimensionPer(self, name, value):
    """Set the value of a fixed coordinate using percentage. value must be between 0 and 1 """
    if not self.mins.has_key(name):
      return
    
    for mysource in self.PXDMFReaders:
      fixeddims = mysource.GetProperty('FixedDimensions')
      names = mysource.GetPropertyValue('PXDMFDimsNameDataInfo')
      for dim in range(len(names)):
        if names[dim] == name:
          fixeddims[dim] = value*(self.maxs[name]-self.mins[name])+self.mins[name]

  def SetFixedDimensionIndexPer(self, index, value):
    """Set the value of a fixed coordinate using percentage. value must be between 0 and 1 """
    if len(self.mins)<= index:
       return
    
    name = self.mins.items()[index][0]
    
    for mysource in self.PXDMFReaders:
      fixeddims = mysource.GetProperty('FixedDimensions')
      names = mysource.GetPropertyValue('PXDMFDimsNameDataInfo')
      for dim in range(len(names)):
        if names[dim] == name:
          fixeddims[dim] = value*(self.maxs[name]-self.mins[name])+self.mins[name]          
          
  def UpdatePipeline(self):
    """Call UpdatePipeline() on each register PXDMFReader"""
    for reader in self.PXDMFReaders:
      reader.UpdatePipeline()
      
  def GetState(self):
    """To recover the state off all the coordinate in a text form. must set the printstate variable to 1 first 
    >>> Pxdmfsync.printstate = 1
    >>> Pxdmfsync.GetState()
    'STATE;:Y:0.0:1.0:X:0.0:3.0;FIN'
    """
    
    if self.printstate:
      self.UpdateSources();
      keys = self.maxs.keys()
      res = "STATE;"
      for key in keys :
        res += ":"+key +":" + str(self.mins[key])+ ":" +  str(self.maxs[key]) 
      res += ";FIN\n"
      self.printstate = 0;
      return res
    return ""
